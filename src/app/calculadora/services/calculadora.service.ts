/**
 * Serviço Responsável por implementar as operações da calculadora.
 * @author Matheus Glauber R. Jordão <glauber.jordao1995@gmail.com>
 * @since 1.0.0
 */

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CalculadoraService {

  /*Definição das constantes usadas para os calculos*/
  static readonly SOMA: string = '+';
  static readonly SUBTRACAO: string = '-';
  static readonly MULTIPLICACAO: string = '*';
  static readonly DIVISAO: string = '/';

  constructor() { }

  /**
   * Método que suporta as quatro operações (adição, subtração, divisão, multiplicação).
   * @param num1 number
   * @param num2 number
   * @param operacao string - Operação a ser executada.
   * @return number - Retorna o valor resultado da operação. 
   */
  calcular(num1: number, num2: number, operacao: string): number{
    let resultado: number;
    
    switch(operacao){
      case CalculadoraService.SOMA:
        resultado = num1 + num2;
      break;
      case CalculadoraService.SUBTRACAO:
        resultado = num1 - num2;
      break;
      case CalculadoraService.MULTIPLICACAO:
        resultado = num1 * num2;
      break;
      case CalculadoraService.DIVISAO:
        resultado = num1 / num2;
      break;
      default:
        return resultado = 0;
    }
    return resultado;
  };
}
