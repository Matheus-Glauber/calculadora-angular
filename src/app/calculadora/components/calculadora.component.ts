import { CalculadoraService } from './../services/calculadora.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calculadora',
  templateUrl: './calculadora.component.html',
  styleUrls: ['./calculadora.component.css']
})
export class CalculadoraComponent implements OnInit {

  private numero1: string;
  private numero2: string;
  private resultado: number;
  private operacao: string;

  constructor(private calculadoraService: CalculadoraService) { }

  ngOnInit() {
    this.limpar();
  }

  /**
   * Método utilizado no ngOnInit, que limpa os dados da calculadora, sempre que é reiniciada.
   * @return void
   */
  limpar(): void{
    this.numero1 = '0';
    this.numero2 = null;
    this.resultado = null;
    this.operacao = null;
  }

  /**
   * Adiciona o número selecionado, para posteriormente calcular.
   * @param numero 
   * @return void
   */
  adicionarNumero(numero: string): void{
    if(this.operacao === null){
      this.numero1 = this.concatenarNumero(this.numero1, numero);
    }else{
      this.numero2 = this.concatenarNumero(this.numero2, numero);
    }
  }
  
  /**
   * Método que realiza a concatenação dos números se necessário.
   * @return void
   * @param numAtual
   * @param numConcat 
   */
  concatenarNumero(numAtual: string, numConcat: string): string{
    // Caso contenha apenas 0 ou null, reinicia o valor.
    if(numAtual === '0' || numAtual === null){
      numAtual = '';
    }
    // primeiro digito é '.', concatena o 0 antes do ponto.
    if(numConcat === "." && numAtual === ''){
      return '0.';
    }
    // caso '.' e já contenha um '.' apenas retorna.
    if(numConcat === '.' && numAtual.indexOf('.') < -1){
      return numAtual
    }
    return numAtual + numConcat
  }

  /**
   * Executa a lógica quando um operador for selecionado.
   * @param operacao
   */
  definirOperacao(operacao: string): void{
    if(this.operacao === null){
      this.operacao = operacao;
      return;
    }
    if(this.numero2 !== null){
      this.resultado = this.calculadoraService.calcular(
        parseFloat(this.numero1),
        parseFloat(this.numero2),
        this.operacao);
      this.operacao = operacao;
      this.numero1 = this.resultado.toString();
      this.numero2 = null;
      this.resultado = null;
    }
  }

  /**
   * Efetuar o calculo da operação.
   * @return void
   */
  calcular(): void{
    if(this.numero2 === null){
      return;
    }

    this.resultado = this.calculadoraService.calcular(
      parseFloat(this.numero1),
      parseFloat(this.numero2),
      this.operacao);
  }

  /**
   * Retorna o valor a ser exibido na tela da calculadora.
   * @return string
   */
  get display(): string{
    if(this.resultado !== null){
      return this.resultado.toString();
    }
    if(this.numero2 != null){
      return this.numero2;
    }
    return this.numero1;
  }
}
